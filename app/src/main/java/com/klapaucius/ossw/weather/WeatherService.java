package com.klapaucius.ossw.weather;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class WeatherService extends Service {

    private final Messenger mMessenger = new Messenger(new Handler());
    private SharedPreferences sharedPref;
    private SharedPreferences.OnSharedPreferenceChangeListener preferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Log.d(" *** weather", "PreferenceChanged: " + key);
            stopTask();
            startTask();
        }
    };
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private void startTask() {
        // Construct an intent that will execute the AlarmReceiver
        Intent intent = new Intent(getApplicationContext(), WeatherUpdateReceiver.class);
        // Create a PendingIntent to be triggered when the alarm goes off
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, WeatherUpdateReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(),
                AlarmManager.INTERVAL_HOUR, pIntent);
//        Log.i(" *** weather", "Language: " + language);
    }

    private void stopTask() {
        Intent intent = new Intent(getApplicationContext(), WeatherUpdateReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, WeatherUpdateReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pIntent);
        Log.d(" *** weather", "Updater task stopped");
    }

    public void onCreate() {
        super.onCreate();
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle("OSSW Weather")
                        .setContentText("Service stopped!");
        int mNotificationId = 2001;
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(" *** weather", "onBind");
        sharedPref.registerOnSharedPreferenceChangeListener(preferenceListener);
        startTask();
        return mMessenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(" *** weather", "onUnbind");
        sharedPref.unregisterOnSharedPreferenceChangeListener(preferenceListener);
        stopTask();
        return super.onUnbind(intent);
    }
}
