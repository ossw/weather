package com.klapaucius.ossw.weather;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class WeatherContentProvider extends ContentProvider {

    static final String AUTHORITY = "com.klapaucius.ossw.weather";
    static final String API_FUNCTIONS_PATH = "api/functions";
    static final String API_PROPERTIES_PATH = "api/properties";
    static final String PROVIDER_PROPERTIES = "properties";

    public static final String LOCATION = "location_value";
    public static final String tDay = "temperature";
    public static final String tMorning = "tempMorning";
    public static final String tEvening = "tempEvening";
    public static final String weatherId = "weatherId";
    public static final String description = "weatherDescription";
    public static final String humidity = "humidity";
    public static final String pressure = "pressure";
    public static final String windSpeed = "windSpeed";
    public static final String date = "date";

    static final Uri PROPERTY_VALUES_URI = Uri.parse("content://" + AUTHORITY + "/" + PROVIDER_PROPERTIES);

    private static final int API_PROPERTIES = 1;
    private static final int API_FUNCTIONS = 2;
    private static final int PROPERTIES = 3;

    private static final String[] PROPERTY_COLUMNS;

    private static final UriMatcher uriMatcher;

    private Map<String, Object> values = new HashMap<>();

    private static class PropertyCard {
        String id;
        String desc;
        String type;

        public PropertyCard(String s1, String s2, String s3) {
            id = s1;
            desc = s2;
            type = s3;
        }
    }

    static List<PropertyCard> properties = new ArrayList<PropertyCard>();

    static void addProperty(String s1, String s2, String s3) {
        properties.add(new PropertyCard(s1, s2, s3));
    }

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, API_PROPERTIES_PATH, API_PROPERTIES);
        uriMatcher.addURI(AUTHORITY, API_FUNCTIONS_PATH, API_FUNCTIONS);
        uriMatcher.addURI(AUTHORITY, PROVIDER_PROPERTIES, PROPERTIES);

        addProperty(LOCATION, "Location city", "STRING");
        addProperty(tDay, "Day temperature", "STRING");
        addProperty(pressure, "Pressure in hPa", "STRING");
        addProperty(humidity, "Humidity", "STRING");
        addProperty(windSpeed, "Wind speed in meters per second", "STRING");
        addProperty(description, "Description", "STRING");
        addProperty(weatherId, "Weather icon code", "INTEGER");

        for (int d = 1; d < 6; d++) {
            addProperty(tDay + d, "Day temperature", "STRING");
            addProperty(tMorning + d, "Morning temperature", "STRING");
            addProperty(tEvening + d, "Evening temperature", "STRING");
            addProperty(pressure + d, "Pressure in hPa", "STRING");
            addProperty(humidity + d, "Humidity", "STRING");
            addProperty(windSpeed + d, "Wind speed in meters per second", "STRING");
            addProperty(description + d, "Description", "STRING");
            addProperty(date + d, "Date", "STRING");
            addProperty(weatherId + d, "Weather icon code", "INTEGER");
        }
        List<String> propColumns = new ArrayList<>();
        for (PropertyCard p : properties) {
            propColumns.add(p.id);
        }
        PROPERTY_COLUMNS = propColumns.toArray(new String[0]);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    static final String API_COLUMN_ID = "_id";
    static final String API_COLUMN_NAME = "name";
    static final String API_COLUMN_DESCRIPTION = "description";
    static final String API_COLUMN_TYPE = "type";

    private static final String[] API_PROPERTY_COLUMNS = new String[]{
            API_COLUMN_ID,
            API_COLUMN_NAME,
            API_COLUMN_DESCRIPTION,
            API_COLUMN_TYPE
    };

    private static final String[] API_FUNCTION_COLUMNS = new String[]{
            API_COLUMN_ID,
            API_COLUMN_NAME,
            API_COLUMN_DESCRIPTION
    };

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[]
            selectionArgs, String sortOrder) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case API_PROPERTIES:
                MatrixCursor cursor = new MatrixCursor(API_PROPERTY_COLUMNS);
                for (PropertyCard p : properties) {
                    addApiPropertyRow(cursor, p.id, p.desc, p.type);
                }
                return cursor;
            case API_FUNCTIONS:
                cursor = new MatrixCursor(API_FUNCTION_COLUMNS);
                return cursor;
            case PROPERTIES:
                String[] columns = projection != null ? projection : PROPERTY_COLUMNS;
                cursor = new MatrixCursor(columns);
                MatrixCursor.RowBuilder rowBuilder = cursor.newRow();
                for (String property : columns) {
                    addPropertyColumn(rowBuilder, property, values.get(property));
                }
                return cursor;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    private void addPropertyColumn(MatrixCursor.RowBuilder rowBuilder, String property, Object value) {
        rowBuilder.add(value);
    }

    private void addApiPropertyRow(MatrixCursor cursor, String property, String desc, String type) {
        cursor.newRow().add(property).add(property).add(desc).add(type);
    }

//    private void addApiFunctionRow(MatrixCursor cursor, SamplePluginFunction function, int descriptionId) {
//        cursor.newRow().add(function.getId()).add(function.getName()).add(getString(descriptionId));
//    }

    private String getString(int stringId) {
        return getContext().getResources().getString(stringId);
    }

    @Override
    public String getType(Uri uri) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case API_PROPERTIES:
                return "vnd.android.cursor.dir/vnd.com.althink.android.ossw.plugin.api.properties";
            case API_FUNCTIONS:
                return "vnd.android.cursor.dir/vnd.com.althink.android.ossw.plugin.api.functions";
            case PROPERTIES:
                return "vnd.android.cursor.item/vnd.com.althink.android.ossw.plugin.properties";
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case PROPERTIES:
                boolean hasChanged = false;
                for (String key : values.keySet()) {
                    Object newValue = values.get(key);
                    Object oldValue = this.values.get(key);
                    if ((oldValue == null && newValue != null) || (oldValue != null && !oldValue.equals(newValue))) {
                        Log.i("weather cp", "Update property '" + key + "' with value: " + newValue);
                        this.values.put(key, values.get(key));
                        hasChanged = true;
                    }
                }
                if (hasChanged) {
                    this.getContext().getContentResolver().notifyChange(uri, null);
                }
                return hasChanged ? 1 : 0;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }
}
