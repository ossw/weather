package com.klapaucius.ossw.weather;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;

public class WeatherHttpClient {
    private static String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?mode=json";
    private static String BASE_FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?mode=json";
    private static String APP_ID = "&APPID=fadaeb371de52b0873c6616f1aa264f9";

    private String buildParameters(String location, String lang, String units) {
        String res = null;
        if (location.matches("[0-9]+"))
            res = "&id=" + location;
        else
            try {
                res = "&q=" + URLEncoder.encode(location, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        return res + "&lang=" + lang + "&units=" + units + APP_ID;
    }

    public String getWeatherData(String location, String lang, String units) {
        String url = BASE_URL + buildParameters(location, lang, units);
//        Log.d("weather url", url);
        return getData(url);
    }

    public String getForecastWeatherData(String location, String lang, String units, int forecastDayNum) {
        String url = BASE_FORECAST_URL + buildParameters(location, lang, units);
        url = url + "&cnt=" + forecastDayNum;
//        Log.d("forecast url", url);
        return getData(url);
    }

    public String getData(String url) {
        HttpURLConnection con = null;
        InputStream is = null;
        try {
            con = (HttpURLConnection) (new URL(url)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.connect();
            // Let's read the response
            StringBuilder buffer = new StringBuilder();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null)
                buffer.append(line).append("\r\n");
            is.close();
            con.disconnect();
            return buffer.toString();
        } catch (IOException t) {
            t.printStackTrace();
        } finally {
            try {
                if (is != null)
                    is.close();
            } catch (IOException t) {
                t.printStackTrace();
            }
            if (con != null)
                con.disconnect();
        }
        return null;
    }
}
