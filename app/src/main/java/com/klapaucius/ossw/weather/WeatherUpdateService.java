package com.klapaucius.ossw.weather;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.survivingwithandroid.weatherapp.JSONWeatherParser;
import com.survivingwithandroid.weatherapp.model.DayForecast;
import com.survivingwithandroid.weatherapp.model.Weather;
import com.survivingwithandroid.weatherapp.model.WeatherForecast;

import org.json.JSONException;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class WeatherUpdateService extends IntentService {

    public WeatherUpdateService() {
        super("WeatherUpdateService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(" *** weather updater", "onHandleIntent -- update service");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String location = sharedPref.getString(WeatherContentProvider.LOCATION, getString(R.string.default_location));
        updateStringProperty(WeatherContentProvider.LOCATION, location);
        String language = sharedPref.getString("weather_language", getString(R.string.default_language));
        String units = sharedPref.getString("weather_units", getString(R.string.default_units));
        String tempSymbol = units.equals(getString(R.string.default_units)) ? "C" : "F";

        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
        symbols.setMinusSign('-');
        NumberFormat formatter0 = new DecimalFormat("#0", symbols);
        NumberFormat formatter1 = new DecimalFormat("#0.0", symbols);
        String data = (new WeatherHttpClient()).getWeatherData(location, language, units);
        if (data == null || data.isEmpty())
            return;
        try {
            Weather weather = JSONWeatherParser.getWeather(data);
//                Log.i(" *** weather", weather.location.getCity() + ", " + weather.location.getCountry());
            updateStringProperty(WeatherContentProvider.tDay, formatter1.format(weather.temperature.getTemp()) + tempSymbol);
            updateStringProperty(WeatherContentProvider.pressure, formatter0.format(weather.currentCondition.getPressure()) + "hPa");
            updateStringProperty(WeatherContentProvider.humidity, formatter0.format(weather.currentCondition.getHumidity()) + "%");
            updateStringProperty(WeatherContentProvider.windSpeed, formatter0.format(weather.wind.getSpeed()) + "m/s");
            updateStringProperty(WeatherContentProvider.description, weather.currentCondition.getDescr());
            updateIntProperty(WeatherContentProvider.weatherId, iconCode(weather.currentCondition.getWeatherId()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        data = (new WeatherHttpClient()).getForecastWeatherData(location, language, units, 5);
        if (data == null || data.isEmpty())
            return;
        try {
            WeatherForecast wf = JSONWeatherParser.getForecastWeather(data);
            for (int d = 1; d < 6; d++) {
                DayForecast df = wf.getForecast(d - 1);
                updateStringProperty(WeatherContentProvider.tDay + d, formatter1.format(df.forecastTemp.day) + tempSymbol);
                updateStringProperty(WeatherContentProvider.tMorning + d, formatter1.format(df.forecastTemp.morning) + tempSymbol);
                updateStringProperty(WeatherContentProvider.tEvening + d, formatter1.format(df.forecastTemp.eve) + tempSymbol);
                updateStringProperty(WeatherContentProvider.pressure + d, formatter0.format(df.weather.currentCondition.getPressure()) + "hPa");
                updateStringProperty(WeatherContentProvider.humidity + d, formatter0.format(df.weather.currentCondition.getHumidity()) + "%");
                updateStringProperty(WeatherContentProvider.windSpeed + d, formatter0.format(df.weather.wind.getSpeed()) + "m/s");
                updateStringProperty(WeatherContentProvider.description + d, df.weather.currentCondition.getDescr());
                updateStringProperty(WeatherContentProvider.date + d, df.getStringDate());
                updateIntProperty(WeatherContentProvider.weatherId + d, iconCode(df.weather.currentCondition.getWeatherId()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int iconCode(int owIconId) {
        if (800 == owIconId) return 0;
        if (801 == owIconId) return 1;
        if ((200 <= owIconId) && (owIconId < 300)) return 9;
        if ((300 <= owIconId) && (owIconId < 400)) return 5;
        if ((500 == owIconId) || (owIconId == 501)) return 3;
        if ((502 <= owIconId) && (owIconId < 600)) return 4;
        if (600 == owIconId) return 6;
        if ((601 == owIconId) || (owIconId == 602)) return 7;
        if ((611 <= owIconId) && (owIconId <= 616)) return 5;
        if ((620 <= owIconId) && (owIconId < 700)) return 7;
        if (906 == owIconId) return 8;
        if ((802 <= owIconId) && (owIconId < 900)) return 2;
        if (900 == owIconId) return 10;
        return 0;
    }

    private void updateStringProperty(String key, String value) {
        ContentValues values = new ContentValues();
        values.put(key, value);
        getContentResolver().update(WeatherContentProvider.PROPERTY_VALUES_URI, values, null, null);
    }

    private void updateIntProperty(String key, int value) {
        ContentValues values = new ContentValues();
        values.put(key, value);
        getContentResolver().update(WeatherContentProvider.PROPERTY_VALUES_URI, values, null, null);
    }
}
