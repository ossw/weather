package com.klapaucius.ossw.weather;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class WeatherUpdateReceiver extends WakefulBroadcastReceiver {
    public static final int REQUEST_CODE = 12345;

    public WeatherUpdateReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(" *** weather receiver", "onReceive -- start update service");
        Intent i = new Intent(context.getApplicationContext(), WeatherUpdateService.class);
        startWakefulService(context.getApplicationContext(), i);
    }
}
