This OSSW extension gets weather data using openweathermap.org API.
Current weather conditions and weather forecast for 5 days are provided.
Weather watch faces and others can be found at: https://github.com/ossw/ossw-sample-watchsets

OSSW extensionId: com.klapaucius.ossw.weather

The following properties are supported in this version:
- temperature
- humidity
- pressure
- windSpeed
- weatherDescription
- weatherId
and for X in {1, 5}
- temperatureX
- tempMorningX
- tempEveningX
- humidityX
- pressureX
- windSpeedX
- weatherDescriptionX
- weatherIdX
- dateX

Weather icons from https://icons8.com/
